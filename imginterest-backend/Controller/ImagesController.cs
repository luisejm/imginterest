﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using imginterest_backend.Models;

namespace imginterest_backend.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private readonly ImageContext _context;

        public ImagesController(ImageContext context)
        {
            _context = context;
        }

        // GET: api/Images
        [HttpGet]
        //[Route("/api/Images")]
        public async Task<ActionResult<IEnumerable<Image>>> GetImages()
        {
            return await _context.Images.ToListAsync();
        }

        // GET: api/Images/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Image>> GetImage(int id)
        {
            var image = await _context.Images.FindAsync(id);

            if (image == null)
            {
                return NotFound();
            }

            return image;
        }


        /* POST: api/Images
        [HttpPost]
        public HttpResponseMessage UploadImage(){
            string imageTag = null;
            var httpRequest = HttpContext.Current.Request;

            //Acá es donde se inicia la subida a la mae (Database)
            var postedFile = httpRequest.Files["Image"];

            //Se crea el archivo de imagen custom
            imageTag = new String(Path.GetFileNameWithoutExtension(postedFile.FileName).Take(10).ToArray()).Replace(" ", "-");
            imageTag = imageTag + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(postedFile.FileName);
            var filePath = HttpContext.Current.Server.MapPath("~/Image/" + imageTag);
            postedFile.SaveAs(filePath);

            //Se agrega la imagen a la puta base de datos que hicimos con cariño
            using (DBModel db = new DBModel()){
                Image image = new Image(){
                    ImageCaption = httpRequest["ImageCaption"],
                    ImageTag = imageTag
                };
                db.Images.Add(image);
                db.SaveChanges();
            }
            return Request.CreateResponse(HttpStatusCode.Created);
        }*/ 
        [HttpPost]
        public async Task<ActionResult<Image>> PostImage(Image image)
        {
            debugger;
            _context.Images.Add(image);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetImage", new { id = image.Id, description = image.Description,
                                                     url = image.Url, isPrivate = image.IsPrivate }, image);
        }

        // DELETE: api/Images/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Image>> DeleteImage(int id)
        {
            var image = await _context.Images.FindAsync(id);
            if (image == null)
            {
                return NotFound();
            }

            _context.Images.Remove(image);
            await _context.SaveChangesAsync();

            return image;
        }

        private bool ImageExists(int id)
        {
            return _context.Images.Any(e => e.Id == id);
        }
    }
}
