﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace imginterest_backend.Migrations.Image
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
     
            migrationBuilder.CreateTable(
                name: "Images",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    Url = table.Column<string>(type: "nvarchar(500)", nullable: false),
                    Tags = table.Column<string>(nullable: false),
                    IsPrivate = table.Column<bool>(nullable: false)
                    /*UserRefId = table.Column<int>(nullable: false)*/
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Images", x => x.Id);
                    /*table.ForeignKey(
                        name: "FK_Images_Users_UserRefId",
                        column: x => x.UserRefId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);*/
                });

            /*  migrationBuilder.CreateIndex(
                name: "IX_Images_UserRefId",
                table: "Images",
                column: "UserRefId");*/
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Images");

        }
    }
}
