using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace imginterest_backend.Models
{
    public class Tag
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}