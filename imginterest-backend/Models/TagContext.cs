using Microsoft.EntityFrameworkCore;

namespace imginterest_backend.Models
{
  public class TagContext : DbContext
  {
    public TagContext(DbContextOptions<TagContext> options)
        : base(options)
    {
    }

    public DbSet<Tag> Tags { get; set; }
  }
}