using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace imginterest_backend.Models
{
    public class Image
    {
        [Key]
        public int Id { get; set; }
        
        public string Description {get; set;}
        public string Url { get; set;}
        public bool IsPrivate {get; set;}

        /* [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string Description { get; set; }

        /*[Required]
        [Column(TypeName = "nvarchar(500)")]
        public string Url {get; set;}
      
        [Required]
        public bool IsPrivate { get; set; }

        /*[ForeignKey("User")]
        public int UserRefId { get; set; }
        public User User { get; set; }*/

    }
}