﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace imginterest_backend.Models
{
    public class TagxImage
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [ForeignKey("Tag")]
       public int Tag_Id { get; set; }
       public Tag Tag { get; set; }

        [Required]
        [ForeignKey("Image")]
        public int Image_Id { get; set; }
        public Image Image { get; set; }
    }
}
