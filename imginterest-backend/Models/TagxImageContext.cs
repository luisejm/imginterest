﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace imginterest_backend.Models
{
    public class TagxImageContext : DbContext
    {
        public TagxImageContext(DbContextOptions<TagxImageContext> options)
      : base(options)
        {
        }

        public DbSet<TagxImage> TagxImage { get; set; }
    }
}
