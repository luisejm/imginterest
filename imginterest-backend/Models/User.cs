using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace imginterest_backend.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string Email { get; set; }
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string FullName {get; set;}
        [Required]
        [Column(TypeName = "nvarchar(100)")]
        public string Password {get; set;}

        public string Token { get; set; }
    }
}