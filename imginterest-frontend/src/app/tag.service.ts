import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(private http: HttpClient) { }

  new(data){
    return this.http.post('http://localhost:5000/api/tags', data,{ headers: new HttpHeaders(this.headers())
    });
  }

  headers() {
    return {
        'Authorization': 'bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : origin,
            'Access-Control-Allow-Credentials': 'true'
    }
  }
}
