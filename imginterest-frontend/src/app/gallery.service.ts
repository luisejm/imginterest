import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  private url: string;
  constructor(private http: HttpClient) {
  	this.url = 'http://localhost:5000/api/Images';
  }

  index(){
  	return this.http.get(this.url);
  }

  create(data){
    debugger;
  	return this.http.post(this.url, data,{ headers: new HttpHeaders(this.headers())
    });
  }

  update(data) {
  	return this.http.put(this.url + "/" + data.id, data);
  }

  delete(id) {
  	return this.http.delete(this.url + "/" + id);
  }

  headers() {
    return {
        'Authorization': 'bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json',
            'Access-Control-Allow-Origin' : origin,
            'Access-Control-Allow-Credentials': 'true'
    }
  }
}