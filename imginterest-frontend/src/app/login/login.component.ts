import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 
  model: any = {};

  constructor(private router: Router, private service: UserService) { }

  ngOnInit() {
  }

  login() {
    this.service.login(this.model)
    .subscribe(res => {
      this.onLoginRedirect();
    });
     }

    onLoginRedirect(): void {
      this.router.navigate(['home/home.component']);
    }

}
