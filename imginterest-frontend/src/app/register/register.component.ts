import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  current_user: User;
  model: any = {};

 constructor(private router: Router,private service: UserService) { }
 
 ngOnInit() {

}

  register() {
    debugger;
    this.service.register(this.model).subscribe(
          data => {
            this.router.navigate(['/login']);
    })
  }
}
