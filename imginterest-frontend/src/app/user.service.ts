import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url: string;
    constructor(private http: HttpClient) {
  	  this.url = 'http://localhost:5000/api/Users';
    }
 
    index(){
      return this.http.get(this.url);
    }

    register(data){
      debugger;
      return this.http.post(this.url + '/Register', data,{ headers: new HttpHeaders(this.headers())
      });
    }

    login(data){
      return this.http.post(this.url + '/Login',data,{ headers: new HttpHeaders(this.headers())
      });
    }

    getUser() {
      return this.http.get(this.url);
    }
    headers() {
      return {
          'Authorization': 'bearer ' + localStorage.getItem('token'),
          'Content-Type': 'application/json',
              'Access-Control-Allow-Origin' : origin,
              'Access-Control-Allow-Credentials': 'true'
      }
    }
}
