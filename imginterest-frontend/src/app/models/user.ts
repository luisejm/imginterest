export class User {
        public id: string;
        public email: string;
        public fullname: string;
        public password: string;
        public token: string;
}
