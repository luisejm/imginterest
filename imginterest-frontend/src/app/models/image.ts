export class Image {

	public id: number;
	public description: string;
    public url: string;
    public isPrivate: boolean;

	constructor(){
		this.isPrivate = false;
	}
}