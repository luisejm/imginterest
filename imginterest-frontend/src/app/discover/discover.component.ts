import { Component, OnInit } from '@angular/core';
import { Image } from '../models/image';
import { GalleryService } from '../gallery.service';
import { TagService } from '../tag.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css']
})
export class DiscoverComponent implements OnInit {

  images: any;
  current_image: Image;
  crud_operation = { is_new: false, is_visible: false };
  tag: any = {};
  

  constructor(private service: GalleryService,private tag_service: TagService,private router: Router) { }

  ngOnInit() {
    this.service.index().subscribe(res => {
      this.images = res;
    });
  }

  new(){
    this.current_image = new Image();
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }

  newTag(){
   this.tag_service.new(this.tag).subscribe(
      data => {
        window.confirm("Created Tag");
     });
     this.current_image = new Image();
     this.crud_operation.is_visible = false;
   };

    public function nameTag() {
        var listElm = document.querySelector('#infinite-list');
        // Add 20 items.
        var nextItem = 1;
        var loadMore = function() {
        for (var i = 0; i < 20; i++) {
        var item = document.createElement('li');
        item.innerText = 'Item ' + nextItem++;
        listElm.appendChild(item);
      }
    }
    // Initially load some items.
    loadMore();

    // Detect when scrolled to bottom.
    listElm.addEventListener('scroll', function() {
      if (listElm.scrollTop + listElm.clientHeight >= listElm.scrollHeight) {
        loadMore();
      }
    })
     
    
}
