import { Component, OnInit } from '@angular/core';
import { Image } from '../models/image';
import { GalleryService } from '../gallery.service';
import {HttpClient} from '@angular/common/http';
import { UploadImageService } from '../shared/upload-image.service';
import { Tag } from '../models/tag';
import { TagService } from '../tag.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  //image: Image;
  image: any = {};
  description: string;
  imgUrl :string = "/assets/img/default.png";
  fileToUpload : File = null;
  model:Tag;
  //current_image: Image;
  crud_operation = { is_new: false, is_visible: false };
  
  constructor(private http: HttpClient, private imageService : UploadImageService,private service: TagService,private serviceimages: GalleryService,private router: Router) {}

  /*handleFileInput(file: FileList){
    this.fileToUpload = file.item(0);

    //Aquí se muestra la imagen en la puta pantalla
    var reader = new FileReader();
    reader.onload = (event:any) => {
      this.imgUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }*/

  onUploadFinish(image: Image){
    this.image = new Image();
    this.image.description = image.description;
    this.image.url_image = image.url;
    /*this.image.isPrivate = image.is_private;*/
  }

  sendImage(image: Image){
    debugger;
    if(this.image != null){
      console.log(event);
      this.http.post('http://localhost:5000/api/Images', {
        description: image.description,
        url: image.url,
        isPrivate: image.isPrivate
      }).subscribe((d) => {
        console.log(d);
      })
    }
  }
  /*constructor(private service: GalleryService) {
    // code...
  }*/

  ngOnInit() {
    /*this.service.index().subscribe(res => {
      this.images = res;
    });*/
  }


  save(){
    debugger;
      this.serviceimages.create(this.image).subscribe(data => {
        window.confirm("Created Image");
        this.router.navigate(['/discover'],data);
      });
   }
}

